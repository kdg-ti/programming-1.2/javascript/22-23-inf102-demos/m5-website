const form = document.forms[0];
const usernameInput = document.getElementById("username");

function setError(input, isOK, message) {
  const errorOut=input.parentElement.querySelector(".error");
  if(!errorOut){
    console.error("errorfield not found");
    return;
  }
  if (isOK){
    errorOut.style.display="none";
  } else{
    errorOut.style.display="block";
    errorOut.textContent = message;
  }
}

function checkUserName() {
  const userName = usernameInput.value;
  const check = userName && /^[a-zA-Z][a-zA-Z0-9]{3}/.test(userName);
  setError(usernameInput,check,"Username should start with a letter and contain at least 4 characters");
  if(check){
    localStorage.setItem('SIGNED_IN_AS', userName);
  }
}

function checkInput() {
  checkUserName()
}

function addEventListeners(){
  form.addEventListener("submit",checkInput);
  usernameInput.addEventListener("blur",checkUserName)
}

addEventListeners()



